




//d3.text(RankURL, function (unparsedData) {
//  barData1 = d3.csv.parseRows(unparsedData);

//});


//    barData=barData1;


function DrawRankChart(width, height) {

    var RankURL = "function/rank.jsp";


    function getColor(sentiment) {
        if (sentiment == 'N') {
            return "#990066";
        } else if (sentiment == 'P') {
            return "#33CC33";
        } else {
            return "#FFCC33";
        }
    }
    function getTextColor(sentiment) {
        if (sentiment == 'N') {
            return "blue";
        } else if (sentiment == 'P') {
            return "#CC6600";
        } else {
            return "purple";
        }
    }


    //var barData = [{ 'sentiment':'P', 'name':'cognition', 'value':0}, {'sentiment':'P', 'name':'food', 'value':0}, {'sentiment':'P', 'name':'time', 'value':1}, {'sentiment':'P', 'name':'person', 'value':1}, {'sentiment':'P', 'name':'QLD', 'value':0}, {'sentiment':'P', 'name':'motion', 'value':0}, {'sentiment':'P', 'name':'artifact', 'value':0}, {'sentiment':'P', 'name':'cognition', 'value':0}, {'sentiment':'P', 'name':'food', 'value':0}, {'sentiment':'P', 'name':'time', 'value':1}, {'sentiment':'P', 'name':'person', 'value':1}, {'sentiment':'P', 'name':'QLD', 'value':0}, {'sentiment':'P', 'name':'motion', 'value':0}, {'sentiment':'P', 'name':'artifact', 'value':0}];
//                xRange = d3.scale.ordinal().rangeRoundBands([MARGINS.left, WIDTH - MARGINS.right], 0.1).domain(barData.map(function (d) {
    //                  return d.name;
    //            })),
            var barPadding = 20;
                    var MARGINS = {
                        top: 50,
                        right: 50,
                        bottom: 20,
                        left: 30
                    };

                    var WIDTH = width - MARGINS.left - MARGINS.right;
                    var HEIGHT = height - MARGINS.top - MARGINS.bottom;


    $(window).on("location_move", function (event, result) {


                    $(".rankloading").empty();
                    var vis = d3.select('.rankloading')
                            .attr("width", WIDTH + MARGINS.left + MARGINS.right)
                            .attr("height", HEIGHT + MARGINS.top + MARGINS.bottom)
                            .attr("transform", "translate(" + MARGINS.top + "," + MARGINS.left + ")");
                    

                    //Set X Scale
                    var numTicks = 5;

        $.get(RankURL, result)
                .done(function (data) {

                    var barData = d3.csv.parse(data.trim());
            
                    var xMax = d3.max(barData, function (d) {
                        return +d.value;
                    });
                    var xMin = 0;
                    var Midwidth=xMax/4;
                    var xScale = d3.scale.linear().range([0, WIDTH- MARGINS.left+2 ]);
                    var xRange = d3.scale.linear().range([0, WIDTH- MARGINS.left+2 ]);
                    
                    var yRange = d3.scale.ordinal().rangeRoundBands([0, HEIGHT], .05);
                    var xAxis = d3.svg.axis().scale(xRange)
                            .orient("top")
                            .tickSize((-height))
                            .ticks(numTicks);
                    
                   var  yAxis = d3.svg.axis().scale(yRange).orient("left").tickSize(0);
                    
                    xRange.domain([xMin, xMax]);
                    //xRange.domain([0, d3.max(barData, function (d) {
//                            return d.value;
  //                      })]);
                    yRange.domain(barData.map(function (d) {
                        return d.name;
                    }));

                    var svg = vis.append("svg")
                            .attr("width", WIDTH)
                            .attr("height", HEIGHT)
                            .attr("class", "base-svg");

                    d3.select(".base-svg").append("text")
                            .attr("x", MARGINS.left+MARGINS.right/2)
                            .attr("y", (MARGINS.top) / 2)
                            .attr("text-anchor", "start")
                            .text("Ranking topics based on Twitter number")
                            .attr("class", "title");

                    svg.append('g')
                            .attr('class', 'x axis')
                            .attr('transform', 'translate(30,' + (MARGINS.top) + ')')
                            .call(xAxis);

                    svg.append('g')
                            .attr('class', 'y axis')
                            //   .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
                            .call(yAxis);

                    var bar = vis.selectAll(".bar")
                            .data(barData)
                            .enter().append("g")
                            .attr("class", "bar")
                            .attr("transform", function (d) {
                                return "translate(0," + yRange(d.name) + ")";
                            });

                    bar.append("rect")
                            .attr("x", MARGINS.left)
                            .attr("y", MARGINS.top)
                            .attr("width", function (d) {
                                return xRange(d.value);
                            })
                            .attr('fill', function (d) {
                                return  getColor(d.sentiment);
                            })
                            .attr("dx", function (d) {
                                return 1;
                            })                            
                            .attr("height",20) //yRange.rangeBand() - barPadding / 2)
                            .on('click', function (d) {
                                var temp = hideCmdId.value;
                                //  alert(temp);
                                //popupWindow = window.open('child_page.html','name','width=200,height=200');,"status=1");
                                mypopupWindow=window.open("showtopicrankword?word=" + encodeURIComponent(d.name) + "&cmd=" + temp, "mywindow", "left=200, top=200, width=1000, height=500","status=1");
                                //window.open("function/showTopic_Rank.jsp?word=" + d.name, "", "left=200, top=200, width=1000, height=500");
                            })

                    bar.append("text")
                            .attr("class", "value")
                            .attr("x", function (d) {
                                //                          if (d.value > Midwidth) 
                                return MARGINS.left * 2;
                                //                              else
//                                    return MARGINS.left + xRange(d.value);
                            })
                            .attr("y", MARGINS.top +10) // yRange.rangeBand() - barPadding)
                            .attr("dx", function (d) {
                                return 1;
                            })
                            .attr('fill', function (d) {
                                return getTextColor(d.sentiment);
                            })
                            .attr("dy", ".35em")
                            //.attr("text-anchor", "end")
                            .attr("text-anchor", "start")
                            .text(function (d) {
                                return d.name;
                            }).on('click', function (d) {
                        var temp = hideCmdId.value;
                        //  alert(temp);
                                mypopupWindow=window.open("showtopicrankword?word=" + encodeURIComponent(d.name) + "&cmd=" + temp, "mywindow", "left=200, top=200, width=1000, height=500","status=1");                        //window.open("function/showTopic_Rank.jsp?word=" + d.name, "", "left=200, top=200, width=1000, height=500");
                    })

                    
                    var grid = xScale.ticks(numTicks);
                    svg.append("g").attr("class", "grid")
                            .selectAll("line")
                            .data(grid, function (d) {
                                return d;
                            })
                            .enter().append("line")
                            .attr("y1", 0)
                            .attr("y2", height + MARGINS.bottom)
                            .attr("x1", function (d) {
                                return xScale(d);
                            })
                            .attr("x2", function (d) {
                                return xScale(d);
                            })
                            .attr("stroke", "white");

                });
    });
}
