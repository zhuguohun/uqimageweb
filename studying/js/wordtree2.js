//Opion

//$(document).ready(function () {
function TreeFunction(width, height) {

    var margin = {top: 20, right: 10, bottom: 20, left: 60};
//    width = $(".wordtree_basic").width() - margin.right - margin.left,
    //          height = $(".wordtree_basic").height() - margin.top - margin.bottom;



    var i = 0,
            duration = 750,
            root;

    var tree = d3.layout.tree()
            .size([height, width]);

    var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });

    var svg_di = d3.select(".wordtree_basic").select("svg");
    if (svg_di != null || svg_di != 'undefined')
        svg_di.remove();

    var svg = d3.select(".wordtree_basic").append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


// alert(width);
    //alert(height);
//json/tree.json
    var MidWidth = 0;
    var widthRate = 0;

    function getColor(sentiment) {
        if (sentiment == 'N') {
            return "#990066";
        } else if (sentiment == 'P') {
            return "#33CC33";
        } else {
            return "#FFCC33";
        }
    }

    d3.json("showtopicstrees", function (error, flare) {
        //  alert(flare);

        if (error)
            throw error;




        root = flare;
        root.x0 = height / 2;
        root.y0 = 100;

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        Midwidth = flare.Weight / 2;
        widthRate = width / (flare.Weight * 3);
        var color = d3.scale.ordinal()
                .range(['#33CC33', '#990066', '#FFCC33']);

        var legend = svg.selectAll(".legend")
                .data(['postive', 'negative', 'neutral'])
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function (d, i) {
                    return "translate(0," + i * 20 + ")";
                });

        legend.append("rect")
                .attr("x", width-margin.right- margin.left-18)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", color)

        legend.append("text")
                .attr("x", width-margin.right- margin.left-24)
                .attr("y", 6)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .style("fill", function (d,i) {
                    if (i==0) return "Orange";
                    else if (i==1) return "blue";
                    return "purple";
                })                
                .text(function (d, i) {                    
                    if (i==0) return "Postive";
                    else if (i==1) return "Negatvie";
                    return "Neutral";
                })


        // root.children.forEach(collapse);
        update(root);


        $("#contentloading").hide();

    });

    d3.select(self.frameElement).style("height", "800px");


    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * width / 8;
        });

        // Update the nodes…
        var node = svg.selectAll("g.TopicHernode")
                .attr("class", "TopicHernode")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });





        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
                .attr("class", "TopicHernode")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", click);

        nodeEnter.append("path")
                .style("stroke", "black")
                .style("fill", "white")
                .attr("d", d3.svg.symbol()
                        .size(200)
                        .type(function (d) {
                            if (checkEmpty(d.children) && checkEmpty(d._children)) {
                                //return "cross";
                                return "diamond";
                            } //else {

                            //}
                        }));

        nodeEnter.append("circle")
                .attr("r", 1e-6)
                .style("fill", function (d) {
                    return d._children ? "lightsteelblue" : "#fff";
                    //    return getColor(d.Opion);
                });

        nodeEnter.append("text")
                .attr("x", function (d) {
                    if (d.Weight > Midwidth)
                        return d.children || d._children ? -13 : 13;
                    else
                        return d.children || d._children ? -13 : 13;
                })
                .attr("dy", function (d) {
                    if (checkEmpty(d.children) && checkEmpty(d._children))
                        return  -5;
                    else
                        return  5;
                })
                .attr("text-anchor", function (d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function (d) {
                    return d.name;
                })
                .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

        nodeUpdate.select("circle")
                .attr("r", function (d) {
                    //       if (d.Weight > Midwidth)
                    //         return d.Weight;
                    //   else
                    //     return d.Weight * 2;
                    if (checkEmpty(d.children) && checkEmpty(d._children)) {
                        return 3;
                    } //else {
                    else
                        return 10;
                })
                .style("fill", function (d) {
                    return getColor(d.Opion);
                    //return d._children ? "lightsteelblue" : "#fff";
                });

        nodeUpdate.select("text")
                .style("fill-opacity", 1)
                .style("fill", function (d) {
                    return getTextColor(d.Opion);
                })
                .text(function (d) {
                    return d.name;
                });
        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

        nodeExit.select("circle")
                .attr("r", 1e-6);

        nodeExit.select("text")
                .style("fill-opacity", 1e-6);

        // Update the links…
        var link = svg.selectAll("path.treelink")
                .data(links, function (d) {
                    return d.target.id;
                });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
                .attr("class", "treelink")
                .style("stroke", function (d) {
                    return d.target.level;
                    //return getTextColor(d.Opion);
                })
                .attr("d", function (d) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal({source: o, target: o});
                });

        // Transition links to their new position.
        link.transition()
                .duration(duration)
                .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {x: source.x, y: source.y};
                    return diagonal({source: o, target: o});
                })
                .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    function checkEmpty(child) {
        return child == null || typeof child == 'undefined';
    }
    function getTextColor(sentiment) {
        if (sentiment == 'N') {
            return "blue";
        } else if (sentiment == 'P') {
            return "#CC6600";
        } else {
            return "purple";
        }
    }

// Toggle children on click.
    function click(d) {


        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }

        if (checkEmpty(d.children) && checkEmpty(d._children)) {
            var currentParent = d.parent;
            var keyname = "";
            var labels = ""; //d.name+":"+d.Opion;           
            keyname = d.name;
            if (d.tw_List < 'undefined')
                labels = d.tw_List;
            while (typeof currentParent != 'undefined') {
                if (currentParent.tw_List < 'undefined')
                    labels += ' ' + currentParent.tw_List;
                //labels += ("," + currentParent.name+":"+currentParent.Opion);
                currentParent = currentParent.parent;
            }

            mypopupWindow=window.open("function/Topic_Key.jsp?keyname=" + keyname + "&word=" + labels, "mywindow", "left=200, top=200, width=1000, height=500","status=1");

        } else {
            update(d);
        }



    }
}
//});

