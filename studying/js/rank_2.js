





function DrawRankChart(width1, height1) {
    var RankURL = "function/rank.jsp";


    function getColor(sentiment) {
        if (sentiment == 'N') {
            return "red";
        } else if (sentiment == 'P') {
            return "green";
        } else {
            return "black";
        }
    }
    function getTextColor(sentiment) {
        if (sentiment == 'N') {
            return "blue";
        } else if (sentiment == 'P') {
            return "#CC6600";
        } else {
            return "purple";
        }
    }


    
        var result = "";


        var margin = {top: 50, bottom: 20, left: 200, right: 40};
        var width = width1 - margin.left - margin.right;
        var height = height1 - margin.top - margin.bottom;

        var xScale = d3.scale.linear().range([0, width]);
        //var yScale = d3.scale.ordinal().rangeRoundBands([0, height], 1.8, 0);
        var yScale = d3.scale.ordinal().rangeRoundBands([0, height], 0.8, 0);
        //var yRange = d3.scale.ordinal().rangeRoundBands([0, HEIGHT], .05);
    
        var numTicks = 5;
        var xAxis = d3.svg.axis().scale(xScale)
                .orient("top")
                .tickSize((-height))
                .ticks(numTicks);
        
        var  yAxis = d3.svg.axis().scale(yScale).orient("left").tickSize(0);
        
        
        var svg = d3.select(".rankloading").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height",height1)// height + margin.top + margin.bottom)
                .attr("class", "base-svg");

        var barSvg = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                .attr("class", "bar-svg");

        var x = barSvg.append("g")
                .attr("class", "x-axis")
                .call(xAxis);

        var y = barSvg.append("g")
                .attr("class", "y-axis")
                .call(yAxis);


        var url = "data.json";
        
$(window).on("location_move", function (event, result) {
        //d3.json(url, function(data) {

        $.get(RankURL, result)
                .done(function (Bardata) {

                    var data = d3.csv.parse(Bardata.trim());
                    var xMax = d3.max(data, function (d) {
                        return d.value;
                    });
                    var xMin = 0;
                    xScale.domain([xMin, xMax]);
                    yScale.domain(data.map(function (d) {
                        return d.name;
                    }));

                    d3.select(".base-svg").append("text")
                            .attr("x", margin.left)
                            .attr("y", (margin.top) / 2)
                            .attr("text-anchor", "start")
                            .text("Ranking the hot topics based on opinion")
                            .attr("class", "title");

                    var groups = barSvg.append("g").attr("class", "labels")
                            .selectAll("text")
                            .data(data)
                            .enter()
                            .append("g");

                    groups.append("text")
                            .attr("x", "0")
                            .attr("y", function (d) {
                                return yScale(d.name);
                            })
                            .text(function (d) {
                                return d.name;
                            })
                            .attr("text-anchor", "end")
                            .attr("dy", ".9em")
                            .attr("dx", "-.32em")
                            .attr("id", function (d, i) {
                                return "label" + i;
                            });

                    var bars = groups
                            .attr("class", "bars")
                            .append("rect")
                            .attr("width", function (d) {
                                return xScale(d.value);
                            })
                            .style("fill", function (d) {
                                return getColor(d.sentiment);
                            })
                            .attr("height", height / 15)
                            .attr("x", xScale(xMin))
                            .attr("y", function (d) {
                                return yScale(d.name);
                            }).on('click', function (d) {
                        var temp = hideCmdId.value;
                        //  alert(temp);
                        window.open("showtopicrankword?word=" + encodeURIComponent(d.name) + "&cmd=" + temp, "", "left=200, top=200, width=1000, height=500");
                        //window.open("function/showTopic_Rank.jsp?word=" + d.name, "", "left=200, top=200, width=1000, height=500");
                    })
                            .attr("id", function (d, i) {
                                return "bar" + i;
                            });

                    groups.append("text")
                            .attr("x", function (d) {
                                return xScale(d.value);
                            })
                            .attr("y", function (d) {
                                return yScale(d.name);
                            })
                            .text(function (d) {
                                return d.value;
                            }).on('click', function (d) {
                        var temp = hideCmdId.value;
                        //  alert(temp);
                        window.open("showtopicrankword?word=" + encodeURIComponent(d.name) + "&cmd=" + temp, "", "left=200, top=200, width=1000, height=500");
                        //window.open("function/showTopic_Rank.jsp?word=" + d.name, "", "left=200, top=200, width=1000, height=500");
                    })
                            .attr("text-anchor", "end")
                            .attr("dy", "1.2em")
                            .attr("dx", "-.32em")
                            .attr("id", "precise-value");

                    bars.on("mouseover", function () {
                        var currentGroup = d3.select(this.parentNode);
                        currentGroup.select("rect").style("fill", "brown");
//                            currentGroup.select("text").style("font-weight", "bold");
                    })
                            .on("mouseout", function () {
                                var currentGroup = d3.select(this.parentNode);
                                currentGroup.select("rect").style("fill", function (d) {
                                    return getColor(d.sentiment);
                                });
                                currentGroup.select("text").style("font-weight", "normal");
                            });

                    //x.call(xAxis);
                    var grid = xScale.ticks(numTicks);
                    barSvg.append("g").attr("class", "grid")
                            .selectAll("line")
                            .data(grid, function (d) {
                                return d;
                            })
                            .enter().append("line")
                            .attr("y1", 0)
                            .attr("y2", height + margin.bottom)
                            .attr("x1", function (d) {
                                return xScale(d);
                            })
                            .attr("x2", function (d) {
                                return xScale(d);
                            })
                            .attr("stroke", "white");

                });


    });
}
