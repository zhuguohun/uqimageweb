<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  	<style>
  		.tweets{
  			border: 3px groove #98bf21;padding:2px;
  			width: 100%;
  			height:100%;
  		}
  		
  		#reportPDF {
  			margin:20px;
  		}
  		
  		#barchart {
  			margin-left: auto;
   			margin-right: auto;
  		}
  		
  		#things {
  			margin-left: auto;
   			margin-right: auto;
  		}
  		
	</style>
  	<script src="./d3/jquery.js"></script>
  	<script src="./d3/d3.js"></script>
    <script src="http://parall.ax/parallax/js/jspdf.js"></script>
    <script src="./d3/html2canvas.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	</head>
  <body>
  	<div id="reportPDF">
  		<div class="container">
  			<div id="things" style="width:713px;">
  			<h2 class="text-center">UQ Tweets Report  <small id="btn" style="font-size: 45%;"><a href="#">PDF</a></small></h2>
  			<div>
  				<p class="lead">From <strong>1 Nov. 2015</strong> to <strong>3 Nov. 2015</strong>, there are <strong>1034234</strong> tweets are collected. <strong>4564</strong> tweets talk about <strong>UQ Final Exam</strong>.</p>
  				<p class="text-center"><strong>Sentiment Analysis for UQ Final Exam</strong></p>
  			</div>
  			
  			
  			<div id="barchart" >
  				
  			</div>
  			<div>
  				<p class="text-success"><strong>Positive</strong></p>
				
				<blockquote>
  					<p>Bismillahirohmanirohim, Welcoming SWOTVAC week, welcoming Final Exam week. May it be the last… (at @uq_news) — https://path.com/p/285iOx </p>
  					<footer>Mutiara Aisyah <cite title="Source Title">Nov 1. 8:52 p.m.</cite></footer>
				</blockquote>
				
				<blockquote class="blockquote-reverse">
  					<p>Final exam today for my MBA students at UQ.  At least one worked out the Tron reference in the sample exam.</p>
  					<footer>Micheal Axelsen <cite title="Source Title">Nov 3. 10:22 a.m.</cite></footer>
				</blockquote>

  			</div>
  			
  			<div>
  				<p class="text-info"><strong>Neutral</strong></p>
  				
  				<blockquote>
  					<p>Uq I will miss you! Finished my final exam in my last semester! #UQ #streetgroove http://instagram.com/p/vPbIMyojKz/ </p>
  					<footer>Bruce Cheung <cite title="Source Title">Nov 1. 8:47 a.m.</cite></footer>
				</blockquote>
				
				<blockquote class="blockquote-reverse">
  					<p>Because final exam is over,because semester break is begin,we are freedom for semester 2 #selfie…</p>
  					<footer>JALON <cite title="Source Title">Nov 1. 9:17 a.m.</cite></footer>
				</blockquote>
				
  			</div>
  			
  			<div>
  				<p class="text-danger"><strong>Negative</strong></p>
  				
  				<blockquote>
  					<p>Its been ages since I sat for a final exam 😔😔 Feeling nervous! First paper and three more to go #Masters #UQ Its a date macroeconomics 😉</p>
  					<footer> Sameer Chand <cite title="Source Title">Nov 2. 9:43 p.m.</cite></footer>
				</blockquote>
				
				<blockquote class="blockquote-reverse">
  					<p>Only one reason I'm here lol. Assignment and Revision for final exam #studyforexam #countdown #exam #stuDYING #UQ </p>
  					<footer>YK <cite title="Source Title">Nov 1. 10:43 p.m.</cite></footer>
				</blockquote>
				
  			</div>
			</div>
		</div>
  	</div>
    <script>
      $("#btn").click(function(e){
      		html2canvas(document.getElementById("things"), 
      			  {"background":"white", pagesplit: true, imageTimeout:2000,removeContainer:true})
      			.then(function(canvas) {
        			var imgData = canvas.toDataURL("image/jpeg", 1.0);
            		var pdf = new jsPDF();
            		pdf.addImage(imgData, 'JPEG', 15, 15);
            		pdf.save("download.pdf");
     			});
      });
      
		var w = 300,                        //width
    		h = 200,                            //height
    		r = 90,                            //radius
    		color = ["#3c763d","#31708f", "#a94442"];     //builtin range of colors

   		 data = [{"label":"Positive", "value":40}, 
            	{"label":"Neutral", "value":40},
            	{"label":"Negative", "value":20}];
    
    var box = $("#barchart");
    box.css("width", w + 10);
    box.css("height", h + 10);
    
    
    var svg = d3.select("#barchart")
        		.append("svg:svg")              //create the SVG element inside the <body>
        		.data([data])                   //associate our data with the document
            	.attr("width", w)           //set the width and height of our visualization (these will be attributes of the <svg> tag
            .attr("height", h)
    
          
	
	
    var vis = svg        
        	.append("svg:g")                //make a group to hold our pie chart
            	.attr("transform", "translate(" + (w / 2 ) + "," + (h / 2) + ")")    //move the center of the pie chart from 0, 0 to radius, radius
	
    var arc = d3.svg.arc()              //this will create <path> elements for us using arc data
        .outerRadius(r);

    var pie = d3.layout.pie()           //this will create arc data for us given a list of values
        .value(function(d) { return d.value; });    //we must tell it out to access the value of each element in our data array

    var arcs = vis.selectAll("g.slice")     //this selects all <g> elements with class slice (there aren't any yet)
        .data(pie)                          //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties) 
        .enter()                            //this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
            .append("svg:g")                //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
                .attr("class", "slice");    //allow us to style things in the slices (like text)

        arcs.append("svg:path")
                .attr("fill", function(d, i) { return color[i]; } ) //set the color for each slice to be chosen from the color function defined above
                .attr("d", arc);                                    //this creates the actual SVG path using the associated data (pie) with the arc drawing function

        arcs.append("svg:text")                                     //add a label to each slice
                .attr("transform", function(d) {                    //set the label's origin to the center of the arc
                //we have to make sure to set these before calling arc.centroid
                d.innerRadius = 0;
                d.outerRadius = r;
                return "translate(" + arc.centroid(d) + ")";        //this gives us a pair of coordinates like [50, 50]
            })
            .attr("text-anchor", "middle")                          //center the text on it's origin
            .text(function(d, i) { return data[i].label; });        //get the label from our original data array
    </script>
  </body>
</html>