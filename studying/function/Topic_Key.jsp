<%@page import="com.mongodb.MongoClientOptions"%>
<%@page import="com.mongodb.DBCursor"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.mongodb.BasicDBObject"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.BasicDBList"%>
<%@ page language="java" %>
<%@ page import ="org.openrdf.repository.*"%>
<%@ page import ="org.openrdf.repository.sparql.*"%>
<%@ page import ="org.openrdf.query.*"%>
<%@ page import ="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.lang.*"%>

<%

    //top right coner
    String word = request.getParameter("word");
    String keyname = request.getParameter("keyname");
    
    if (keyname!=null) keyname=keyname.trim();    
    
    //else word="QLD,University,UQ";
    String []tempword=word.split(" ");
   // System.out.println(tempword.length);    
    
        DBObject postcodequery = null;// 
        int page_Number=25;
        int cur_num=0;
            BasicDBList postcode_or = new BasicDBList();
            for (String temps : tempword) {
                if (temps==null) continue;
                if (temps.length()<2)continue;
                DBObject clause4072 =null;
                if (temps.charAt(0)<'0'||temps.charAt(0)>'9')
                    clause4072=new BasicDBObject("tweet_ID", temps);
                else
                    clause4072=new BasicDBObject("tweet_ID", Long.valueOf(temps));
                //    out.println("tid="+temps+"<br>");
                postcode_or.add(clause4072);
                if (cur_num>page_Number) break;
            }
        postcodequery = new BasicDBObject("$or", postcode_or);
        
            //if (postcodequery!=null) andor.add(postcodequery);
        // Enable MongoDB logging in general
    //    System.setProperty("DEBUG.MONGO", "false");
        // Enable DB operation tracing
     //   System.setProperty("DB.TRACE", "false");

            //DBObject query = new BasicDBObject("$and", andor);
        MongoClientOptions mco = new MongoClientOptions.Builder()
        .connectionsPerHost(100).threadsAllowedToBlockForConnectionMultiplier(10).build();
        
        MongoClient mongo = new MongoClient("localhost", mco);
        
//        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("test");

        DBCollection Twcollection = db.getCollection("search_tw_fb");

        DBCursor cursor = null;
        if (postcodequery != null) {
            cursor = Twcollection.find(postcodequery);
        } else {
            out.println("error on query");
        }

    
%>            

<h1>Search hot topics: <%=keyname%>  results : <%=cursor.count() %> items </h1>
         
<table class='Result_table' border="1">
    <tr>
        <th>Post_Id</th>
        <th>Publish Time</th>
        <th>Content</th>        
    </tr>
    <%
        while (cursor.hasNext()) {

            DBObject data = cursor.next();
            Long tweet_id = (Long) data.get("tweet_ID");
            String screenName = (String) data.get("user_name");
            //     screenName = screenName.toLowerCase();
            String text1 = (String) data.get("old_text");
            Long publicationTime = (Long) data.get("LongCreateTime");
            String postcode = (String) data.get("R_Post_Code");
            String myOpion = (String) data.get("opinion");
            String color = "white";
            if (myOpion.charAt(0) == 'P') {
                color = "green";
            } else if (myOpion.charAt(0) == 'N') {
                color = "red";
            }
%>                
  
    <tr>
        <td bgcolor = "<%=color %>" > <%=postcode  %></td>
        <td bgcolor = "<%=color %>" > <%=new Date(publicationTime)  %></td>

        <td><a href="https://twitter.com/guohun/status/<%=tweet_id%>" > <%=text1%> </a> </td>
    </tr>                    
    <%
        }
        cursor.close();
        Twcollection=null;
        db=null;
    %>
</table>

