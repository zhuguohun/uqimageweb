<%@page import="com.mongodb.Bytes"%>
<%@ page language="java" %>
<%@page import="com.mongodb.DBCursor"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.BasicDBObject"%>
<%@page import="com.mongodb.BasicDBList"%>
<%@page import="com.mongodb.MongoClient"%>
<%@ page import ="org.openrdf.repository.*"%>
<%@ page import ="org.openrdf.repository.sparql.*"%>
<%@ page import ="org.openrdf.query.*"%>
<%@ page import ="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.lang.*"%>
<%@include file="inc.jsp"%>
<%    //top right coner
    String word1 = request.getParameter("userName");
    String word = word1.replace("'", "");

    if (word != null) {
        word = word.trim();
    } else {
        word = "UQ";
    }
    //bottom left coner		
    //numofresult
    String numOfResult = request.getParameter("numOfResult");
    if (numOfResult != null) {
        //top right coner
        String x1 = request.getParameter("x1");
        String y1 = request.getParameter("y1");
        //bottom left coner		
        String x2 = request.getParameter("x2");
        String y2 = request.getParameter("y2");
    } else {
        numOfResult = "50";
    }

    DBObject clause2 = new BasicDBObject("user_name", word);
    BasicDBList andor = new BasicDBList();
    andor.add(clause2);
    DBObject query = new BasicDBObject("$and", andor);

    MongoClient mongo = new MongoClient("localhost", 27017);
    DB db = mongo.getDB("test");

    DBCollection Twcollection = db.getCollection("search_tw_fb");


%>

<h1>Search Twitter or Facebook for user:  <font color='blue'>  <%=word1%>   </font> results.</h1> 

<table class='Result_table' border="1">
    <tr>
        <th>Post_Id</th>
        <th>Publish Time</th>
        <th>Content</th>        
    </tr>
    <%
        DBCursor cursor = Twcollection.find(query);//.addOption(Bytes.QUERYOPTION_NOTIMEOUT)  //
                    //.addOption(Bytes.QUERYOPTION_AWAITDATA);
            ;//.sort(new BasicDBObject("tweet_ID", 1));
        System.out.println("get data \t" + cursor.count());
    
        while (cursor.hasNext()) {
            
            DBObject data = cursor.next();
            Long v_user_Id = null;
            try {
                v_user_Id = (Long) data.get("user_Id");
            } catch (java.lang.ClassCastException ex) {
                Integer v_user_Id1 = (Integer) data.get("user_Id");
                v_user_Id = Long.valueOf(v_user_Id1);
            }
            if (v_user_Id == null) {
                continue;
            }
            Long tweet_id = (Long) data.get("tweet_ID");
            String screenName = (String) data.get("user_name");
            screenName = screenName.toLowerCase();
            String text1 = (String) data.get("tweet_text");
            Long publicationTime = (Long) data.get("LongCreateTime");
            String postcode = (String) data.get("R_Post_Code");
            String myOpion = (String) data.get("opinion");
            Double lati = (Double) data.get("Latitude");
            Double longi = (Double) data.get("Longitude");
            String color = "black";
            if (myOpion.charAt(0) == 'P') {
                color = "blue";
            } else if (myOpion.charAt(0) == 'N') {
                color = "red";
            }

    %>                
    <tr  bgcolor="<%=color%>" >
        <td><%=postcode%></td>
        <td><%=new Date(publicationTime)%></td>

        <td>
            <a href="https://twitter.com/guohun/status/<%=tweet_id%>" > <%=text1%> </a> </td>
    </tr>                    
    <%

            cursor.close();
        }
        // BufferStr.setLength(BufferStr.length() - 2);
        //out.print(BufferStr.toString());
    %>
</table>

