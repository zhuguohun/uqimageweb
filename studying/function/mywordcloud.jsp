<%-- 
    Document   : mywordcloud
    Created on : 05/09/2015, 12:25:21 PM
    Author     : uqgzhu1
--%>

<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.apache.http.HttpResponse"%>
<%@page import="org.apache.http.client.methods.HttpGet"%>
<%@page import="org.apache.http.impl.client.DefaultHttpClient"%>
<%@page import="uqimagePkg.WordCloudList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import ="org.openrdf.repository.*"%>
<%@ page import ="org.openrdf.repository.sparql.*"%>
<%@ page import ="org.openrdf.query.*"%>
<%@include file="inc.jsp"%>

<%    String highstr = request.getParameter("h");
    String widthstr = request.getParameter("w");
    int G_High = 450;
    int G_Width = 450;
    if (highstr != null) {
        G_High = Integer.valueOf(highstr);
        G_Width = Integer.valueOf(widthstr);
    }

    double x1 = -27.49;
    double x2 = -27.550;
    double y1 = 153.1;
    double y2 = 152.3;

    String x1S = request.getParameter("x1");
    if (x1S != null) {
        String y1S = request.getParameter("y1");
        String x2S = request.getParameter("x2");
        String y2S = request.getParameter("y2");

        x1 = Double.valueOf(x1S);
        x2 = Double.valueOf(x2S);
        y1 = Double.valueOf(y1S);
        y2 = Double.valueOf(y2S);

    }

//    String cmd = (String) session.getAttribute("cmd");
    
        ServletContext tempSession = this.getServletConfig().getServletContext();        
        //String menuCmd = (String) tempSession.getAttribute("cmd");
        String cmd = (String) tempSession.getAttribute("cmd");

    if (cmd==null||(!cmd.contains("safety")&&!cmd.contains("services"))) {        
        //4064
        util.FoursquareAPI PostAPI = new util.FoursquareAPI();
        String[] searchPost = PostAPI.getAuStateCity(x1, y1, x2, y2);//);
        PostAPI.Close();
        if (searchPost == null) {
            return;
        }

        if (searchPost.length < 1) {
            System.out.println("---------------Ranking--no results------------ ");
            return;
        }

        StringBuffer query = null;
        if (cmd == null) {
            query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/rootRest/resources/getWordcloud?postcode=");
        } else {
        switch (cmd.charAt(0)) {
            case 'l':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/livingRest/resources/getWordcloud?postcode=");                
                break;
            case 'n':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/negativeRest/resources/getWordcloud?postcode=");                
                break;            
            case 'm':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/simpleRest/resources/getWordcloud?postcode=");                
                break;
            case 'p':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/postiveRest/resources/getWordcloud?postcode=");                                break;                
            case 'r':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/rootRest/resources/getWordcloud?postcode=");
                break;                
            case 'R':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/researchRest/resources/getWordcloud?postcode=");
                break;                
                
            case 's':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/studyRest/resources/getWordcloud?postcode=");
                break;
            case 't':
                query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/teachingRest/resources/getWordcloud?postcode=");                
                break;
                
        }
        }
//        query.append(G_High);
        //      query.append("&width=");
        //    query.append(G_Width);
        //  query.append('&');
        query.append(searchPost[0]);
        for (int i = 1; i < searchPost.length; i++) {
            query.append(",");
            query.append(searchPost[i]);
        }
        query.append(",");
        query.append("0000");

        //out.println(query);
        //System.out.println(query);
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(query.toString());
        getRequest.addHeader("accept", "text/html");

        HttpResponse response1 = httpClient.execute(getRequest);

        if (response1.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response1.getStatusLine().getStatusCode());
        }

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response1.getEntity().getContent())));

        String output;
        System.out.println("Output from Server .... \n");
        int i = 0;
        StringBuffer tempStr = new StringBuffer();
        while ((output = br.readLine()) != null) {
            //out.println(output);
            tempStr.append(output);
            tempStr.append('\n');
            i++;
            if (i > 35) {
                break;
            }
        }
        br.close();
        httpClient.getConnectionManager().shutdown();

        out.println(tempStr);

        return;
    }

    util.FoursquareAPI PostAPI = new util.FoursquareAPI();
    String[] searchPost = PostAPI.getAuStateCity(x1, y1, x2, y2);//);
    PostAPI.Close();
    if (searchPost == null) {
        return;
    }
    if (searchPost.length < 1) {
        System.out.println("------------wordcloud -----no results------------ ");
        return;
    }

    StringBuffer query = new StringBuffer(" PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
            + "PREFIX geo: <http://twitter.com/geolocation/#> "
            + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
            + "PREFIX et: <http://twitter.com/Event/#> "
            + "PREFIX st: <http://twitter.com/Cloud/#> "
            + " select ?a1 ?term  where { ?a1  a foaf:mbox.  ?a1 st:HighF ?term.  ");

    for (int i = 0; i < searchPost.length; i++) {
        String tempx = searchPost[i];
        query.append("{ ?a1 rdfs:label '");
        query.append(tempx);
        System.out.println(tempx);
        if (i < searchPost.length - 1) {
            query.append("' } union  ");
        } else {
            query.append("'} } limit 400");
        }
    }

    SPARQLRepository myRepository = new SPARQLRepository(sesameServer);
    myRepository.initialize();
    //querying the repository
    RepositoryConnection con = null;
    con = myRepository.getConnection();

    String queryString = query.toString();
    String TwitterHead = "http://twitter.com/";
//    StringBuffer BufferStr = new StringBuffer();
//     	out.print(queryString);
    //BufferStr.append("['ID', 'date', 'Hotest', 'Region','impact'],");
    WordCloudList TermsList = new WordCloudList();
    TermsList.setShowSize(G_Width, G_High);

    try {
        String Twrequest = query.toString();
//        out.println(Twrequest);
        TupleQuery MytupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, Twrequest);

        //MytupleQuery.setMaxQueryTime(180);
        TupleQueryResult res = MytupleQuery.evaluate();
        String checker = "";

        while (res.hasNext()) {
            BindingSet bs = res.next();

            String termStr = bs.getBinding("term").getValue().stringValue();

            String[] strArry = termStr.split(",");
            for (String tokenStr : strArry) {
                String words[] = tokenStr.split(":");
                if (words[0] == null || words[0].length() < 2) {
                    continue;
                }
//                 out.println("<H2>"+words[0]+":"+words[1]+":"+words[2]+"</h2>");
                TermsList.add(words);
            }
        }
        res.close();
        TermsList.sortToken();
    } catch (RepositoryException ex) {
        System.out.println(ex);
    } catch (MalformedQueryException ex) {
        System.out.println(ex);
    } catch (QueryEvaluationException ex) {
        System.out.println(ex);
    } finally {
        try {
            if (con != null) {
                con.close();
            }
        } catch (RepositoryException ex) {
            System.out.println(ex);
        }
    }
    // BufferStr.setLength(BufferStr.length() - 1);
//    BufferStr.append("");
    StringBuffer BufferStr = new StringBuffer("weight,color,token\n");

    for (int i = 0; i < TermsList.size(); i++) {
        BufferStr.append(TermsList.getWeight(i));
        BufferStr.append(",");
        BufferStr.append(TermsList.getColor(i));
        BufferStr.append(",");
        BufferStr.append(TermsList.getToken(i));
        BufferStr.append("\n");
        if (i > 30) {
            break;
        }
    }
    out.print(BufferStr.toString());
%>