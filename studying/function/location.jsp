<%@page import="org.apache.http.HttpResponse"%>
<%@page import="org.apache.http.client.methods.HttpGet"%>
<%@page import="org.apache.http.params.HttpConnectionParams"%>
<%@page import="org.apache.http.impl.client.DefaultHttpClient"%>
<%@page import="org.apache.http.client.HttpClient"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@ page language="java" %>
<%@ page import ="org.openrdf.repository.*"%>
<%@ page import ="org.openrdf.repository.sparql.*"%>
<%@ page import ="org.openrdf.query.*"%>
<%@ page import ="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.lang.*"%>

<%@include file="inc.jsp"%>

<%    

   //String x1 = request.getParameter("x1");
    //String y1 = request.getParameter("y1");
    //bottom left coner		
    //String x2 = request.getParameter("x2");
    //String y2 = request.getParameter("y2");
    double x1 = -27.49;
    double x2 = -27.550;
    double y1 = 153.1;
    double y2 = 152.3;

    String x1S = request.getParameter("x1");
    if (x1S != null) {
        String y1S = request.getParameter("y1");
        String x2S = request.getParameter("x2");
        String y2S = request.getParameter("y2");

        x1 = Double.valueOf(x1S);
        x2 = Double.valueOf(x2S);
        y1 = Double.valueOf(y1S);
        y2 = Double.valueOf(y2S);
        System.err.println("x1,y1,x2,y2=(\t"+x1+","+y1+","+x2+","+y2+"\t)");
    }
    //numofresult
    String numOfResult = request.getParameter("numOfResult");

    //String sesameServer = "http://flashlite.rcc.uq.edu.au:8080/openrdf-sesame/repositories/uqidb";
//    String cmd = (String) session.getAttribute("cmd");
        ServletContext tempSession = this.getServletConfig().getServletContext();        
        //String menuCmd = (String) tempSession.getAttribute("cmd");
        String cmd = (String) tempSession.getAttribute("cmd");

    if (cmd==null||(!cmd.contains("safety")&&!cmd.contains("services"))) {

        util.FoursquareAPI PostAPI = new util.FoursquareAPI();
        String[] searchPost = PostAPI.getAuStateCity(x1, y1, x2, y2);//);
        PostAPI.Close();
        if (searchPost == null) {
            return;
        }

        if (searchPost.length < 1) {
            System.out.println("---------------Ranking--no results------------ ");
            return;
        }

        StringBuffer query = null;
        if (cmd == null) {
            query=new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/rootRest/resources/getMap?postcode=");
        } else {
            switch (cmd.charAt(0)) {
                case 'l':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/livingRest/resources/getMap?postcode=");
                    break;
                case 'n':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/negativeRest/resources/getMap?postcode=");
                    break;
                
                case 'm':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/simpleRest/resources/getMap?postcode=");
                    break;
                case 'p':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/postiveRest/resources/getMap?postcode=");
                    break;                    
                case 'r':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/rootRest/resources/getMap?postcode=");
                    break;                    
                case 's':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/studyRest/resources/getMap?postcode=");
                    break;
                case 'R':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/researchRest/resources/getMap?postcode=");
                    break;
                case 't':
                    query = new StringBuffer("http://flashlite.rcc.uq.edu.au:8080/teachingRest/resources/getMap?postcode=");
                    break;
                  
            }
        }

        boolean post4068=false;
        boolean post4072=false;
        for (int i = 0; i < searchPost.length; i++) {
            if (i!=0)query.append(",");
            query.append(searchPost[i]);
            if (searchPost[i].contains("4072")) post4072=true;
            else if (searchPost[i].contains("4608")) post4068=true;
        }
        if (post4072!=true) {query.append(",");             query.append("4072");}
        if (post4068!=true) {query.append(",");             query.append("4068");}

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(query.toString());
        getRequest.addHeader("accept", "text/html");

        HttpResponse response1 = httpClient.execute(getRequest);

        if (response1.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response1.getStatusLine().getStatusCode());
        }

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response1.getEntity().getContent())));

        String output;
        //System.out.println("Output from Server .... \n");
        StringBuffer tempStr = new StringBuffer();
        while ((output = br.readLine()) != null) {
            //out.println(output);
            tempStr.append(output);
            tempStr.append('\n');
        }
        br.close();
        httpClient.getConnectionManager().shutdown();
        out.println(tempStr);
        /*
         URL url = new URL(query.toString());
         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
         conn.setConnectTimeout(1000);
         conn.setRequestMethod("GET");
         //StringBuffer BufferStr = new StringBuffer();
         if (conn.getResponseCode() != 200) {
         throw new RuntimeException("Failed : HTTP error code : "
         + conn.getResponseCode());
         }

         BufferedReader br = new BufferedReader(new InputStreamReader(
         (conn.getInputStream())));

         String output;		
         while ((output = br.readLine()) != null) {
         out.println(output);
         }

         conn.disconnect();
         */
        //http://localhost:8080/simpleRest/resources/getMap?postcode=4064,4072,4067
        return;
    }

    SPARQLRepository myRepository = new SPARQLRepository(sesameServer);
    myRepository.initialize();
    //querying the repository
    RepositoryConnection con = null;
    con = myRepository.getConnection();
    String prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
            + "PREFIX tw: <http://twitter.com/>"
            + "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
            + "PREFIX geo:<http://twitter.com/geolocation/#>"; //replace prefix         

    //query change 
    String query = " SELECT DISTINCT ?lat ?longi ?name ?text ?tid WHERE {   ?url tw:Twitt ?tid  . ?tid rdfs:comment ?text .  ?url rdfs:label ?name . "
            + " ?tid geo:longi ?longi. ?tid geo:lat ?lat";

    if (x1S != null) {
        query = query + ".FILTER(?longi>" + y2 + " && ?longi<" + y1 + " && ?lat>" + x2 + " && ?lat<" + x1 + ") }  LIMIT " + numOfResult;
    } else {
        query = query + " } limit 20";
    }

    String queryString = prefix + query;
    String TwitterHead = "http://twitter.com/";
    StringBuffer BufferStr = new StringBuffer();
    try {
        String request1 = prefix + query;
        {
            //out.println(request1);
            int Edgenum = 0;
            int LikeEdgenum = 1;
            String Twrequest = request1;
            TupleQuery MytupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, Twrequest);

            TupleQueryResult res = MytupleQuery.evaluate();
            String checkExit = "";

            while (res.hasNext()) {
                BindingSet bs = res.next();
                String _DPostName = bs.getBinding("tid").getValue().stringValue();
                if (checkExit != _DPostName) {
                    checkExit = _DPostName;
                    String temp = bs.getBinding("text").getValue().stringValue();
                    StringBuffer myTest1 = new StringBuffer(temp.replaceAll(",", "."));
                    Binding tempLa = bs.getBinding("lat");
                    // if (tempLa==null ) { out.println(_DPostName+"has null lat"); continue;}                  
                    String _Lat = tempLa.getValue().stringValue();
                    String _longi = bs.getBinding("longi").getValue().stringValue();
                    if (_DPostName == null) {
                        continue;
                    }
                    int loc = _DPostName.lastIndexOf("/");
                    String DPostName = _DPostName.substring(loc + 1);

                    BufferStr.append("'");
                    BufferStr.append(DPostName);
                    BufferStr.append("',");
                    BufferStr.append(Double.valueOf(_Lat));
                    //BufferStr.append(Math.random() * ( Double.valueOf(x1) - Double.valueOf(x2) )+Double.valueOf(x2));
                    BufferStr.append(",");
                    BufferStr.append(Double.valueOf(_longi));
                    // BufferStr.append(Math.random() * ( Double.valueOf(y1) - Double.valueOf(y2) )+Double.valueOf(y2));
                    //		BufferStr.append(",'");
                    Edgenum++;
                    //   BufferStr.append(TwitterHead+DPostName);
                    //   BufferStr.append("','");
                    //	BufferStr.append(myTest1);
                    BufferStr.append("\n");
                }
            }
            res.close();
        }
    } catch (RepositoryException ex) {
        System.out.println(ex);
    } catch (MalformedQueryException ex) {
        System.out.println(ex);
    } catch (QueryEvaluationException ex) {
        System.out.println(ex);
    } finally {
        try {
            if (con != null) {
                con.close();
            }
        } catch (RepositoryException ex) {
            System.out.println(ex);
        }
    }
    // BufferStr.setLength(BufferStr.length() - 2);
    out.print(BufferStr.toString());
%>



