<%-- 
    Document   : mybignetwork
    Created on : 03/11/2015, 10:03:51 PM
    Author     : uqgzhu1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta charset="utf-8">
<style>

    .node {
        stroke: #fff;
        stroke-width: 1.5px;
    }

    .link {
        stroke: #999;
        stroke-opacity: .6;
    }

</style>
<body>
    <div id="container" class="container">
        <div id="sidebar" >
            <div class="item-group">
                <label class="item-label">Filter</label>
                <div id="filterContainer" class="filterContainer checkbox-interaction-group"></div>
            </div>
        </div>
        <div id="graphContainer" class="graphContainer"></div>
    </div>

    <script src="../d3/d3.min.js"></script>
    <script src="../d3/jquery.js"></script>
    <script>

        
        createFilter();
        showGraph("",'');
        
        function showGraph(uniName, showorhidden) {
            var SURL = "./network.jsp";
            var width = 1260,
                    height = 960;

            var color = d3.scale.category20();

            var force = d3.layout.force()
                    .charge(-120)
                    .linkDistance(30)
                    .size([width, height]);
            
            //d3.select(".graphContainer").empty();
            $(".graphContainer").empty();
            var svg = d3.select(".graphContainer").append("svg")
                    .attr("width", width)
                    .attr("height", height);
            
            if (uniName!=""){
                SURL=SURL+"?uniname="+uniName+"&sh="+showorhidden;
            }
            //alert(SURL);
            d3.json(SURL, function (error, graph) {
                if (error)
                    throw error;

                force
                        .nodes(graph.nodes)
                        .links(graph.links)
                        .start();

                var link = svg.selectAll(".link")
                        .data(graph.links)
                        .enter().append("line")
                        .attr("class", "link")
                        .style("stroke-width", function (d) {
                            return Math.sqrt(d.value);
                        });

                var node = svg.selectAll(".node")
                        .data(graph.nodes)
                        .enter().append("circle")
                        .attr("class", "node")
                        .attr("name", function (d) {
                            return d.name;
                        })
                        .attr("r", 5)
                        .style("fill", function (d) {
                            return color(d.group);
                        })
                        .on('click', function () {
                            window.open("http://twitter.com/" + d3.select(this).attr("name"), "User Profile", "left=300, top=200, width=600, height=500");
                            d3.event.stopPropagation();
                        })
                        .call(force.drag);

                node.append("title")
                        .text(function (d) {
                            return d.name;
                        });

                force.on("tick", function () {
                    link.attr("x1", function (d) {
                        return d.source.x;
                    })
                            .attr("y1", function (d) {
                                return d.source.y;
                            })
                            .attr("x2", function (d) {
                                return d.target.x;
                            })
                            .attr("y2", function (d) {
                                return d.target.y;
                            });

                    node.attr("cx", function (d) {
                        return d.x;
                    })
                            .attr("cy", function (d) {
                                return d.y;
                            });
                });
            });
        }
// Method to create the filter
        

        // Method to create the filter, generate checkbox options on fly
        function createFilter() {
            d3.select(".filterContainer").selectAll("div")
                    .data(["UQ", "ANU", "MEL"])
                    .enter()
                    .append("div")
                    .attr("class", "checkbox-container")
                    .append("label")
                    .each(function (d) {
                        // create checkbox for each data
                        d3.select(this).append("input")
                                .attr("type", "checkbox")
                                .attr("id", function (d) {
                                    return "chk_" + d;
                                })
                                .attr("checked", true)
                                .on("click", function (d, i) {
                                    // register on click event
                                    var lVisibility = this.checked ? 't' : 'f';
                                    //alert(d + "\t" + lVisibility);
                                    showGraph(d,lVisibility);
                                    //var lVisibility = this.checked ? "visible" : "hidden";
                                    //filterGraph(d, lVisibility);
                                })
                        d3.select(this).append("span")
                                .text(function (d) {
                                    return d;
                                });
                    });
            //$("#sidebar").show();
        }
    </script>