<%@page import="com.mongodb.BasicDBObject"%>
<%@page import="com.mongodb.BasicDBList"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.DBCursor"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.mongodb.MongoClientOptions"%>
<%@ page language="java" %>
<%@ page import ="org.openrdf.repository.*"%>
<%@ page import ="org.openrdf.repository.sparql.*"%>
<%@ page import ="org.openrdf.query.*"%>
<%@ page import ="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.lang.*"%>
<%@include file="inc.jsp"%>
<%    //top right coner
    String word1 = request.getParameter("word");
    if (word1==null) word1="research";           
    String word = word1.replace("'", "");

    if (word != null) {
        word = word.trim();
    } else {
        word = "UQ";
    }
    String eventname = request.getParameter("eventName");

    String prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
            + "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n"
            + "PREFIX dc: <http://purl.org/dc/elements/1.1/> \n"
            + " PREFIX et: <http://twitter.com/Event/#> \n"
            + "PREFIX st: <http://twitter.com/Cloud/#> \n"
            + "prefix tw: <http://twitter.com/> \n";

    String query = "select ?starttime ?endtime ?items   where { "
            + "?eventname et:tim ?starttime. "
            + "?eventname et:etim ?endtime.  "
            + " ?eventname foaf:mbox ?postcode.  "
            + " ?postcode rdfs:label  '" + word + "'."
            + " ?eventname et:assi ?items."
            + " ?eventname rdfs:label '" + eventname + "'."
            + "} limit 100";

    //+ "  filter regex(?term,'" + word + "','i')  }";
    // out.println(query);
    SPARQLRepository myRepository = new SPARQLRepository(EventServer);
    myRepository.initialize();
    //querying the repository
    RepositoryConnection con = null;
    con = myRepository.getConnection();

    TupleQuery MytupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, prefix + query);
    TupleQueryResult res = MytupleQuery.evaluate();

    String[] twitter_idS = null;
    Date Starttime = null;
    Date Endtime = null;
    if (res.hasNext()) {
        BindingSet bs = res.next();
        Starttime = new Date(Long.valueOf(bs.getBinding("starttime").getValue().stringValue()));
        Endtime = new Date(Long.valueOf(bs.getBinding("endtime").getValue().stringValue()));
        String items = bs.getBinding("items").getValue().stringValue();
        twitter_idS = items.split(" ");
    }
    con.close();

    if (twitter_idS == null) {
%>
<h1>Event:  <font color='blue'>  <%=eventname%>   </font> </h1> 
<h2> Database may be    Could not find support Evidences, please contact Administration.</h2> 
<%
        return;
    }

    //DBObject query = new BasicDBObject("$and", andor);
    MongoClientOptions mco = new MongoClientOptions.Builder()
            .connectionsPerHost(100).threadsAllowedToBlockForConnectionMultiplier(10).build();

    MongoClient mongo = new MongoClient("localhost", mco);

//        MongoClient mongo = new MongoClient("localhost", 27017);
    DB db = mongo.getDB("test");

    DBCollection Twcollection = db.getCollection("search_tw_fb");

    DBCursor cursor = null;

    DBObject postcodequery = null;// 
    int page_Number = 25;
    int cur_num = 0;
    BasicDBList postcode_or = new BasicDBList();
    for (String temps : twitter_idS) {
        if (temps == null) {
            continue;
        }
        if (temps.length() < 2) {
            continue;
        }
        DBObject clause4072 = null;
        if (temps.charAt(0) < '0' || temps.charAt(0) > '9') {
            clause4072 = new BasicDBObject("tweet_ID", temps);
        } else {
            clause4072 = new BasicDBObject("tweet_ID", Long.valueOf(temps));
        }
        //    out.println("tid="+temps+"<br>");
        postcode_or.add(clause4072);
        if (cur_num > page_Number) {
            break;
        }
    }
    postcodequery = new BasicDBObject("$or", postcode_or);

    if (postcodequery != null) {
        cursor = Twcollection.find(postcodequery);
    } else {
        out.println("error on query");
    }
    int numberOftwitters = cursor.count();

%>            


<h1>Event:  <font color='blue'>  <%=eventname%>   </font> results.</h1> 
<h2> Start: <font color='blue'>   <%=Starttime%> </font> and  End: <font color='blue'> <%=Endtime%> </font> </h2> <br> <h2>Total twitter: <font color='blue'>    <%=numberOftwitters%></font></h2>. 
<table class='Result_table' border="1">
    <tr>
        <th>Post_Id</th>
        <th>Publish Time</th>
        <th>Content</th>        
    </tr>
    <%
        while (cursor.hasNext()) {

            DBObject data = cursor.next();
            Long tweet_id = (Long) data.get("tweet_ID");
            String screenName = (String) data.get("user_name");
            //     screenName = screenName.toLowerCase();
            String text1 = (String) data.get("old_text");
            Long publicationTime = (Long) data.get("LongCreateTime");
            String postcode = (String) data.get("R_Post_Code");
            String myOpion = (String) data.get("opinion");
            String color = "white";
            if (myOpion.charAt(0) == 'P') {
                color = "green";
            } else if (myOpion.charAt(0) == 'N') {
                color = "red";
            }
    %>                

    <tr>
        <td bgcolor = "<%=color%>" > <%=postcode%></td>
        <td bgcolor = "<%=color%>" > <%=new Date(publicationTime)%></td>

        <td><a href="https://twitter.com/guohun/status/<%=tweet_id%>" > <%=text1%> </a> </td>
    </tr>                    
    <%
        }
        cursor.close();
        Twcollection = null;
        db = null;
    %>
</table>


