<%-- 
    Document   : makeSearch
    Created on : 01/09/2015, 9:04:20 PM
    Author     : uqgzhu1
--%>

<%@ page import="java.io.*,java.util.*,javax.mail.*"%>
<%@ page import="javax.mail.internet.*,javax.activation.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<HTML> 
    <link href="./css/main_1.css" rel="stylesheet" type="text/css" >    
    <BODY> 
        <%@include file="header.html" %>
        <%

            // Get username. 
            //String email = request.getParameter("email");
            String email = (String) session.getAttribute("userEmail");
            if (email == null) {
                email = "g.zhu@uq.edu.au";
            }
            String myTextinput = request.getParameter("emailcontent");


        %> 


        <% if (myTextinput == null || myTextinput.equals("")) { %> 

        <h1> Please enter an email address again.  </h1>

        <%
                return;
            }

        %> 



        <div class="wrapper"> 
            <div class="tab-content">
                <h2>Thank you for your feedback: </h2>
                <p></p>
                <p></p>
                <p></p>
                : <%= myTextinput%> 

                <h2>
                    You are made it at <%= new java.util.Date()%>     
                </h2>


                <%
                    String result = "<font color='red'> Error: unable to send message.... </font>";
                    if (email != null && !email.equals("")) {

                        // Recipient's email ID needs to be mentioned.
                        String to[] ={"xueli@itee.uq.edu.au","g.zhu@uq.edu.au","u1017412@gmail.com","tony.guohun.zhu@gmail.com"};
                        String from = email;
                        String host = "smtp.uq.edu.au";

                        Properties properties = System.getProperties();
                        properties.setProperty("mail.smtp.host", host);
                        Session mailSession = Session.getDefaultInstance(properties);

                        try {
                            // Create a default MimeMessage object.
                            MimeMessage message = new MimeMessage(mailSession);
                            // Set From: header field of the header.
                            message.setFrom(new InternetAddress(from));
                            // Set To: header field of the header.
                            message.addRecipient(Message.RecipientType.TO,
                                    new InternetAddress(to[0]));
                            message.addRecipient(Message.RecipientType.CC,
                                    new InternetAddress(to[1]));
                            
                            message.addRecipient(Message.RecipientType.BCC,
                                    new InternetAddress(to[2]));
                            
                            // Set Subject: header field
                            message.setSubject("UQ Image Feedback!");

                            // Send the actual HTML message, as big as you like
                            message.setContent("<h2>" + myTextinput + "</h2>",
                                    "text/html");
                            // Send message
                            Transport.send(message);
                            result = "<font color='#0033CC'>  A confirmation email has been sent from your email .." + email + " <br>Thank you again.</font>";
                        } catch (MessagingException mex) {
                            mex.printStackTrace();
                        }
                    }
                    out.println("<br><h2>" + result + "</h2>");
                %>             
            </div>  
            <br>
            <br>            
            <h2>
                You will jump to <a href="http:///studying/studyng.jsp">Home page after 5 seconds</a>
            </h2>
        </div>
        <%@include file="footer.html" %>

        <script type="text/javascript">
        window.setTimeout(function(){
            window.location.href = "/studying/studying.jsp";
        }, 5000);        
            </script>

    </BODY> 
</HTML> 